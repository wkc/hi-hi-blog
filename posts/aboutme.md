title : About Me
-------------
LINKS & CONTACT

-   Blog: http://blog.hi-hi.cn
-   GitHub: https://github.com/wangkechun
-   Git@OSC: http://git.oschina.net/wkc
-   Email: hi@hi-hi.cn
-   QQ: 978750903


-   我的强项:`python`(我的最爱),javascript(`coffeescript`,`angularjs`)
-   `超强的自学能力`
-   良好的代码风格,喜欢重构代码,喜欢函数式编程
-   两年acm经历(acm集训队副队长),良好的算法能力
-   独立服务器,vps,bae,sae等部署管理经验(科大online juage管理员)
-   熟练html/css/bootstrap/`angularjs`/nodejs/jquery. 有丰富的`单页面web应用`开发经验,写过企业项目(微信网页&pc管理&手机短信&打印机,已上线),也有过用户过万的小项目.
-   熟悉 git&svn 版本控制，使用github托管代码,`有丰富的基于git的团队协作开发经验`
-   常年混迹于github,持续关注各种新技术的发展
-   熟悉python标准库,flask框架,ipython notebook. web开发,爬虫,科学计算,图像处理(验证码识别)都不在话下
-   了解抓包技术，能进行简单协议分析,`写过无数爬虫` (requests,scrapy,urllib2,mongodb,xpath,css选择器,正则)
-   熟料使用chrome开发者工具,写过自己的chrome扩展
-   了解c/c++/java/php/golang/易语言...,掌握基本语法
-   ubuntu忠实用户, 长年用虚拟机跑windows
-   掌握科学上网技术，墙外的资料查找查看无压力
-   熟悉`mongodb`(诸多项目经验),管理过mysql数据库


- SKILLS:
    - `python` :
        - flask
        - requests
        - ipython,`ipython notebook`
        - scrapy
    - web :
        - `angularjs`
        - node.js
        - `coffeescript`
        - jquery
        - `underscore`
        - `bootstrap`
        - tool :
            - grunt
            - bower
            - koala
            - fiddler
            - chrome:
                - chrome扩展开发
                - `chrome开发者工具`
        - safe :
            - XSS
            - CRFS
            - 注入
    - `golang`  
    - java :
        - `groovy` 学习中
        - `scala` 学习中
    - PHP
    - C++,C : 两年acm经历
    - linux:
        - `ubuntu`
        - zsh + oh-my-zsh
        - ipython
        - ssh 隧道 vpn 代理
    - windows:
        - 易语言
        - ahk
    - edit:
        - `sublime text`
        - vim
    - datebase:
        - `mongodb`
        - mysql
        - sqlite
        - redis
    - ide :
        - visual studio
        - eclipse
    - 云平台:
        - sae
        - bae
        - 七牛
        - digitalocean vps
    - 版本控制:
        - `git`
            - github
            - oschina
        - svn
            - sae svn

- My Friends
  -   http://vgee.cn/
  -   http://qious.cn/
