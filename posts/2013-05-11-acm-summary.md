title: 市赛总结
description: 
category: acm
tag: acm
hide:true
-------------------------

说实话这一次的结果还是有点小小的遗憾的,别人同样四个题有两个银牌，我们却只有两个铜牌。罚时太多了。

比赛开始首先以为A题是水题，做了几分钟发现不对劲，而且几个队伍多错了，马上换题。然后开F题，wzh继续DFS　A题。I题由于我对军棋的规则不熟。写得比较慢，还好wzh给的样例,但是由于忘了删调试代码错了一次。汗。让后wzh敲A题DP。fjy写H题三分。然后D题以为是AC自动机，放弃。E题找到了模板，但不肯定。而且代码太长，直到比赛结束还没有敲完。然后A题交错了改了很久最后记忆化搜索才通过，但倒数第二次提交鼠标滑了一下交成了JAVA。暴汗。这题比赛结束后居然一下子想到了DP公式，比赛时硬是没有想到。这时时间已经只有两个小时。我们才做了两个题目。然后开F题，模拟。还是我写代码，写完了发现样例不对。然后调试，结果编译器出故障了。二位数组总是无法显示。奇葩的错误。然后浪费了很多时间。最后重新建工程才解决。但样例还是过不了。 怀疑数据有问题。到web board看了下。原来样例错了。而且我一开始写的代码就是对的。白白浪费了这么多时间。交了之后发现封榜了。

然后fjy三分的问题还在WA中。让我看一下。我随口说端点特殊处理一下。结果对了。汗。。然后还有20分钟。选中最后一题。试图暴力。做最后的挣扎。但是到比赛结束也没有敲完。以四个题结束了比赛。结束后榜又开放了。看了下排名23。总共89个队伍。在25%左右。

感想：
主要是经验不足。本来遇到编译器出问题早该重启编译器的，比赛是却一直以为是代码问题，找了很久。发现样例可能有问题也是想了很久才想到看board的。还有调试代码。千万不要为了省事不用debug宏包围的。这几个问题都出现在我的身上。我自己得好好反省。唯一值得庆幸的是我们的配合还算不错。相互之间帮忙解决了很多问题。

> 嘉杰信息”杯2013年ACM/ICPC湘潭多省程序设计竞赛暨第五届湘潭市大学生程序设计竞赛
> 
> 2013年5月11日
